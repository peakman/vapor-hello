//
//  AppDirectTests.swift
//  AppTests
//
//  Created by Steve Clarke on 09/02/2019.
//
/*
@testable import App
import XCTest
import PerfectCURL
import Vapor

let port = 8282
let host = String("localhost")
let scheme = "http"


func makeUrlComponents() -> URLComponents {
    var urlComps = URLComponents()
    urlComps.host = host
    urlComps.port = port
    urlComps.scheme = scheme
    return urlComps
}
func makeProductionComponents() -> URLComponents {
    var urlComps = URLComponents()
    urlComps.host = "schello.herokuapp.com"
    urlComps.scheme = "https"
    return urlComps
}
struct ErrorResponse : Decodable {
    var error: Bool
    var reason: String
}

struct Todo : Codable {
    var id : UUID?
    var title : String
}

class AppDirectTests: XCTestCase {
    var urlBaseComponents : URLComponents!
    var urlBase : URL!
    var todouuid : String!
    static var serverStarted = false
    
    override func setUp() {
        todouuid = String(UUID())
        let env : String
        if let envs = UserDefaults.standard.string(forKey: "env") {
            env = envs
        } else {
            //fatalError("env argument is missing")
            env = "development"
        }
        if env != "production" {
            urlBaseComponents = makeUrlComponents()
            if !AppDirectTests.serverStarted {
                AppDirectTests.serverStarted = true
                DispatchQueue.global().async {
                    do {
                        let command = UserDefaults.standard.string(forKey: "command") ?? "serve"
                        var args : [String] = [command]
                        if command == "serve" {
                            args.append(contentsOf: ["-H", host, "--port" , String(port)])
                        }
                        let helloApp = try app(Environment(name: env, isRelease: false, arguments: args))
                        try helloApp.run()
                    } catch  {
                        fatalError("Error running app: \(error)")
                    }
                }
            }
        } else {
            urlBaseComponents = makeProductionComponents()
        }
        urlBase = urlBaseComponents.url!
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testBasicSync() throws {
        print("about to send request for testBasicSync to \(urlBase.absoluteString)")
        let resp = try? CURLRequest(urlBase.absoluteString).perform()
        if resp == nil {
            print("error sending testBasicSync request")
            XCTFail()
        } else {
            let rs = String(resp!.bodyString)
            print("Response: \(rs)")
            XCTAssertEqual(resp!.responseCode, 200)
        }
    }
    
    func testBasicAsync() throws {
        let theExpectation = expectation(description: "code 200")
        print("about to send request for testBasicAsync to \(urlBase.absoluteString)")
        CURLRequest(urlBase.absoluteString).perform() { resp in
            var resp1 : CURLResponse
            do {
                resp1 = try resp()
                let rs = String(resp1.bodyString)
                print("Response: \(rs)")
                XCTAssertEqual(resp1.responseCode, 200)
            } catch {
                XCTFail()
            }
            theExpectation.fulfill()
        }
        waitForExpectations(timeout: 30, handler: nil)
    }
    func testFailedUpdateTodoWithEncodedForm() throws {
        var resp : CURLResponse? = nil
        urlBaseComponents.path = "/todos"
        let urls : URL? = urlBaseComponents.url
        guard urls != nil else {fatalError("Cannot gernerate url")}
        let request  =   CURLRequest(urls!.absoluteString,
                                     //.failOnError,
                                     .httpMethod(.post),
                                     .postField(.init(name: "id", value: todouuid)),
                                     .postField(.init(name: "title", value: "Important Stuff!")))
        
        do {
            resp = try request.perform()
            guard resp != nil else {XCTFail(); return}
            let respj = try resp!.bodyJSON(ErrorResponse.self )
            XCTAssert(resp!.responseCode == 500)
            XCTAssert(respj.reason.hasPrefix("Attempt to update"))
        } catch {
            print("curl request failed", error.localizedDescription)
        }
    }
    func testCreateTodoWithEncodedForm() throws {
        var resp : CURLResponse? = nil
        urlBaseComponents.path = "/todos"
        let urls : URL? = urlBaseComponents.url
        guard urls != nil else {fatalError("Cannot gernerate url")}
        let request  =   CURLRequest(urls!.absoluteString,
                                     .failOnError,
                                     .httpMethod(.post),
                                     .postField(.init(name: "title", value: "Important Stuff!")))
        
        do {
            resp = try request.perform()
            print("Response:", resp!.bodyString)
            resp = try? CURLRequest(urls!.absoluteString,.addHeader(.accept, "application/json; charset=utf-8")).perform()
            guard resp != nil else {XCTFail(); return}
            print("body string: \(resp!.bodyString)")
            let todos : [Todo]
            do {
                todos = try resp!.bodyJSON([Todo].self)
            } catch {
                print("error is : \(error.localizedDescription)")
                XCTFail(); return
            }
            XCTAssertEqual(todos.count, 0)
            
        } catch {
            print("curl request failed", error.localizedDescription)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
    func testTodos() throws {
        print("about to send request for testTodos to")
        urlBaseComponents.path = "/todos"
        let urls = urlBaseComponents.url
        guard urls != nil else {XCTFail(); return}
        let resp = try? CURLRequest(urls!.absoluteString,.addHeader(.accept, "application/json; charset=utf-8")).perform()
        guard resp != nil else {XCTFail(); return}
        print("body string: \(resp!.bodyString)")
        let todos : [Todo]
        do {
            todos = try resp!.bodyJSON([Todo].self)
        } catch {
            print("error is : \(error.localizedDescription)")
            XCTFail(); return
        }
        XCTAssertEqual(todos.count, 0)
    }

    func testMessageWithURLQuery() throws {
        var resp : CURLResponse? = nil
        let fnItem = URLQueryItem(name: "firstName", value: "Alex (boy)")
        let lnItem = URLQueryItem(name: "lastName", value: "Smith")
        let agItem = URLQueryItem(name: "age", value: "72")
        urlBaseComponents.path = "/message"
        urlBaseComponents.queryItems = [fnItem, lnItem, agItem]
        let urls : URL? = urlBaseComponents.url
        guard urls != nil else {fatalError("Cannot gernerate url")}
        let request  =   CURLRequest(urls!.absoluteString, .failOnError)
        do {
            resp = try request.perform()
            print("Response:", resp!.bodyString)
        } catch {
            print("curl request failed", error)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
    func testMessageWithEncodedForm() throws {
        var resp : CURLResponse? = nil
        let urls = urlBase.appendingPathComponent("message", isDirectory: true).absoluteString
        let request  =   CURLRequest(urls, .failOnError,
                                     .postField(.init(name: "firstName", value: "Steve")),
                                     .postField(.init(name: "lastName", value: "FORM")),
                                     .postField(.init(name: "age", value: "72"))
        )
        do {
            resp = try request.perform()
            print("Response:", resp!.bodyString)
        } catch {
            print("curl request failed", error)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
    func testMessageWithJSON() throws {
        var resp : CURLResponse? = nil
        let urls = urlBase.appendingPathComponent("message", isDirectory: true).absoluteString
        let names = Names(firstName: "Susie", lastName: "JSON", age: 72)
        let json = try JSONEncoder().encode(names)
        let request  =   CURLRequest(urls, .failOnError,
                                     .postString(String(data: json, encoding: .utf8)!),
                                     .addHeader(.contentType, "application/json; charset=utf-8"),
                                     .addHeader(.accept, "application/json; charset=utf-8")
        )
        do {
            resp = try request.perform()
            print("Response:", resp!.bodyString)
            let names = try resp!.bodyJSON(Names.self)
            print("First name is ", names.firstName, "\nAge is ", names.age)
        } catch {
            print("curl request failed", error)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
    func testMessage() throws {
        var resp : CURLResponse? = nil
        do {
            let urls = urlBase.appendingPathComponent("message", isDirectory: true).absoluteString
            resp = try CURLRequest(urls).perform()
            print("Response:", resp!.bodyString)
        } catch {
            print("curl request failed", error)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
}
 */



