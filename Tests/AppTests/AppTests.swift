import XCTest
import PostgreSC
import PerfectCURL

let port = 8282
let host = String("localhost")
let scheme = "http"

let bootedString = "**-BOOTED-**"

struct Todo : PGCodable {
    var id : UUID?
    var title : String
    init(id: UUID, title: String) {
        self.id = id
        self.title = title
    }
}
struct ErrorResponse : Decodable {
    var error: Bool
    var reason: String
}

struct Names : Codable {
    var firstName: String
    var lastName: String
    var age: Int
}


func freeServerPort() {
    let killTask = try? NSUserUnixTask(url: FileManager.default.homeDirectoryForCurrentUser.appendingPathComponent("Development/scbin/k\(port)"))
    guard killTask != nil else {
        fatalError("Unable to create killTask")
    }
    killTask?.execute() { comp in
        if comp == nil  {
            print("completion condition of killTask: \(String(describing: comp))")
        }
    }
}


func readForBooted(pipe: Pipe, semaphore: DispatchSemaphore) {
    print("Reading for booted")
    let fh = pipe.fileHandleForReading
    var data : Data
    data = fh.availableData
    var serverString = String()
    var booted = false
    let outputFileName = FileManager.default.homeDirectoryForCurrentUser.appendingPathComponent("/temp/DO_NOT_DELETE/hello_output.text")
    guard FileManager.default.createFile(atPath: outputFileName.path, contents: nil, attributes: nil)  else {fatalError("Unable to create output file \(outputFileName)")}
    let outputFile = try? FileHandle(forWritingTo: outputFileName)
    while data.count > 0 {
        outputFile!.write(data)
        outputFile?.synchronizeFile()
        let str = String(data: data, encoding: .utf8)
        guard str != nil else {fatalError("Should have got a string!")}
        //print("about to get available data")
        data = fh.availableData
        serverString += str!
        let outputLines : [String] = serverString.split(separator: "\n").map() {String($0)}
        let bootLine =  outputLines.first() {$0 == bootedString}
        if !booted && bootLine != nil {
            semaphore.signal()
            booted = true
            print("Server has booted")
        }
    }
}

func launchServer( ) -> NSUserUnixTask {
    let semaphore = DispatchSemaphore(value: 0)
    var debugDir  = Bundle.allBundles.first() {
        if let epath = $0.executablePath  {
                return epath.contains(string: "/Debug/")
        } else {
            return false
        }
    }?.executableURL
    guard debugDir != nil else {fatalError("Did not find path to server executable")}
    while debugDir!.lastPathComponent != "Debug" {
        debugDir = debugDir!.deletingLastPathComponent()
    }
    let testOutputDir = URL(fileURLWithPath: #file, isDirectory: false).deletingLastPathComponent().deletingLastPathComponent().appendingPathComponent("TestOutput", isDirectory: true)
    print("This file is in directory:", testOutputDir.absoluteString)
    let task = try? NSUserUnixTask(url: debugDir!.appendingPathComponent("RunCopy", isDirectory: false))
    guard task != nil else {fatalError("Unable to init NSUSERUNIXTASK")}
    let stdPipe = Pipe()
    let errorFileName = FileManager.default.homeDirectoryForCurrentUser.appendingPathComponent("/temp/DO_NOT_DELETE/hello_errors.text", isDirectory: false)
    guard FileManager.default.createFile(atPath: errorFileName.path, contents: nil, attributes: nil)  else {fatalError("Unable to create error file \(errorFileName)")}
    let errFile = try? FileHandle(forWritingTo: errorFileName)
    guard errFile != nil else {fatalError("Unable to create errFile")}
    task!.standardOutput = stdPipe.fileHandleForWriting
    task!.standardError = errFile!
    let background = DispatchQueue(label: "hello")
    let command = UserDefaults.standard.string(forKey: "command") ?? "serve"
    let env = UserDefaults.standard.string(forKey: "env") ?? "development"
    var args = [command, "--env", env]
    if command == "serve" {
        args.append(contentsOf: ["-H", host, "--port" , String(port)])
    }
    task!.execute(withArguments: args) { completionError in
        if let error = completionError {
            fputs("***** ERROR *******\nServer completed with: \(error.localizedDescription)\nSee error output file.\n", stderr)
        }
    }
    let launchTime = DispatchTime.now().uptimeNanoseconds
    background.async {
        readForBooted(pipe: stdPipe, semaphore: semaphore)
    }
    let res = semaphore.wait(timeout: DispatchTime(uptimeNanoseconds: launchTime + UInt64(exactly:  2e9 )!))
    if res == DispatchTimeoutResult.timedOut {
        fatalError("Timed out waiting for server to boot")
    }
    
    print("Server launched")
    return task!
}

func makeUrlComponents() -> URLComponents {
    var urlComps = URLComponents()
    urlComps.host = host
    urlComps.port = port
    urlComps.scheme = scheme
    return urlComps
}


final class AppSeparateTests: XCTestCase {
    var urlBaseComponents : URLComponents!
    var connection : Connection!
    var urlBase : URL!
    var todouuid : String!
    static var task : NSUserUnixTask? = nil
    var task : NSUserUnixTask? {get{return AppSeparateTests.task} set(t) {AppSeparateTests.task = t}}
    
    override func setUp() {
        let dbType = UserDefaults.standard.string(forKey: "dbType") ?? "hellodev"
        connection = Database.connect(dbType: dbType)
        todouuid = UUID().description
        urlBaseComponents = makeUrlComponents()
        urlBase = urlBaseComponents.url!
        guard task == nil else { return }
        freeServerPort()
        task = launchServer()
    }
    func terminateServer() {
        task = nil
    }

    
    func testBasicSync() throws {
        print("about to send request for testBasicSync to \(urlBase.absoluteString)")
        let resp = try? CURLRequest(urlBase.absoluteString).perform()
        if resp == nil {
            print("error sending testBasicSync request")
            XCTFail()
        } else {
            let rs = String(resp!.bodyString)
            print("Response: \(rs)")
            XCTAssertEqual(resp!.responseCode, 200)
        }
    }
    
    func testBasicAsync() throws {
        let theExpectation = expectation(description: "code 200")
        print("about to send request for testBasicAsync to \(urlBase.absoluteString)")
        CURLRequest(urlBase.absoluteString).perform() { resp in
            var resp1 : CURLResponse
            do {
                resp1 = try resp()
                let rs = String(resp1.bodyString)
                print("Response: \(rs)")
                XCTAssertEqual(resp1.responseCode, 200)
            } catch {
                XCTFail()
            }
            theExpectation.fulfill()
        }
        waitForExpectations(timeout: 30, handler: nil)
    }
    func testFailedUpdateTodoWithEncodedForm() throws {
        var resp : CURLResponse? = nil
        urlBaseComponents.path = "/todos"
        let urls : URL? = urlBaseComponents.url
        guard urls != nil else {fatalError("Cannot gernerate url")}
        let request  =   CURLRequest(urls!.absoluteString,
                                     //.failOnError,
            .httpMethod(.post),
            .postField(.init(name: "id", value: todouuid)),
            .postField(.init(name: "title", value: "Important Stuff!")))
        
        do {
            resp = try request.perform()
            guard resp != nil else {XCTFail(); return}
            let respj = try resp!.bodyJSON(ErrorResponse.self )
            XCTAssert(resp!.responseCode == 500)
            XCTAssert(respj.reason.hasPrefix("Attempt to update"))
        } catch {
            print("curl request failed", error.localizedDescription)
        }
    }
    func testCreateTodoWithEncodedForm() throws {
        var resp : CURLResponse? = nil
        urlBaseComponents.path = "/todos"
        let urls : URL? = urlBaseComponents.url
        guard urls != nil else {fatalError("Cannot gernerate url")}
        _ = connection.execute(query: "delete from todo")
        let request  =   CURLRequest(urls!.absoluteString,
                                     .failOnError,
                                     .httpMethod(.post),
                                     .postField(.init(name: "title", value: "Important Stuff!")))
        
        do {
            resp = try request.perform()
            print("Response:", resp!.bodyString)
            resp = try? CURLRequest(urls!.absoluteString,.addHeader(.accept, "application/json; charset=utf-8")).perform()
            guard resp != nil else {XCTFail(); return}
            print("body string: \(resp!.bodyString)")
            let todos : [Todo]
            do {
                todos = try resp!.bodyJSON([Todo].self)
            } catch {
                print("error is : \(error.localizedDescription)")
                XCTFail(); return
            }
            XCTAssertEqual(todos.count, 1)
            
        } catch {
            print("curl request failed", error.localizedDescription)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
    func testTodos() throws {
        print("about to send request for testTodos to")
        _ = connection.execute(query: "delete from todo")
        let todo = Todo(id: UUID(), title: "Yet another title")
        let dbname = UserDefaults.standard.string(forKey: "dbname") ?? "hello"
        let encoder = PGEncoder<Todo>(dbname: dbname,table: "todo")
        let newI : UUID  = todo.insert(on: connection, using: encoder)

        urlBaseComponents.path = "/todos"
        let urls = urlBaseComponents.url
        guard urls != nil else {XCTFail(); return}
        let resp = try? CURLRequest(urls!.absoluteString,.addHeader(.accept, "application/json; charset=utf-8")).perform()
        guard resp != nil else {XCTFail(); return}
        print("body string: \(resp!.bodyString)")
        let todos : [Todo]
        do {
            todos = try resp!.bodyJSON([Todo].self)
        } catch {
            print("error is : \(error.localizedDescription)")
            XCTFail(); return
        }
        XCTAssertEqual(todos.count, 1)
        let decoder = PGDecoder<Todo>(connection: connection, dbname: "hello", table: "todo", query: nil, notNull: nil)
        for todo in decoder {
            print("Todo from database: ",todo)
            XCTAssertEqual(todo.id!, newI)
        }
    }
    
    func testMessageWithURLQuery() throws {
        var resp : CURLResponse? = nil
        let fnItem = URLQueryItem(name: "firstName", value: "Alex (boy)")
        let lnItem = URLQueryItem(name: "lastName", value: "Smith")
        let agItem = URLQueryItem(name: "age", value: "72")
        urlBaseComponents.path = "/message"
        urlBaseComponents.queryItems = [fnItem, lnItem, agItem]
        let urls : URL? = urlBaseComponents.url
        guard urls != nil else {fatalError("Cannot gernerate url")}
        let request  =   CURLRequest(urls!.absoluteString, .failOnError)
        do {
            resp = try request.perform()
            print("Response:", resp!.bodyString)
        } catch {
            print("curl request failed", error)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
    func testMessageWithEncodedForm() throws {
        var resp : CURLResponse? = nil
        let urls = urlBase.appendingPathComponent("message", isDirectory: true).absoluteString
        let request  =   CURLRequest(urls, .failOnError,
                                     .postField(.init(name: "firstName", value: "Steve")),
                                     .postField(.init(name: "lastName", value: "FORM")),
                                     .postField(.init(name: "age", value: "72"))
        )
        do {
            resp = try request.perform()
            print("Response:", resp!.bodyString)
        } catch {
            print("curl request failed", error)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
    func testMessageWithJSON() throws {
        var resp : CURLResponse? = nil
        let urls = urlBase.appendingPathComponent("message", isDirectory: true).absoluteString
        let names = Names(firstName: "Susie", lastName: "JSON", age: 72)
        let json = try JSONEncoder().encode(names)
        let request  =   CURLRequest(urls, .failOnError,
                                     .postString(String(data: json, encoding: .utf8)!),
                                     .addHeader(.contentType, "application/json; charset=utf-8"),
                                     .addHeader(.accept, "application/json; charset=utf-8")
        )
        do {
            resp = try request.perform()
            print("Response:", resp!.bodyString)
            let names = try resp!.bodyJSON(Names.self)
            print("First name is ", names.firstName, "\nAge is ", names.age)
        } catch {
            print("curl request failed", error)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
    func testMessage() throws {
        var resp : CURLResponse? = nil
        do {
            let urls = urlBase.appendingPathComponent("message", isDirectory: true).absoluteString
            resp = try CURLRequest(urls).perform()
            print("Response:", resp!.bodyString)
        } catch {
            print("curl request failed", error)
        }
        XCTAssertNotNil(resp)
        XCTAssertEqual(resp?.responseCode, 200)
    }
}

final class KillServer: XCTestCase {
    override func tearDown() {
        freeServerPort()
    }
    func testNothing() {
    }
}
