import Vapor
import FluentSQLite
import FluentPostgreSQL

final  class RouterWrapper<DB> where DB:  SchemaSupporting & MigrationSupporting {
    let router: Router
    init(router: Router, db: DB) {
        self.router = router
    }
    func routes() throws {
        // Basic "It works" example
        router.get { (req) -> String in
            let logger = try? req.make(Logger.self)
            logger?.log(req.debugDescription, at: .verbose, file: #file, function: #function, line: #line, column: #column)
            return "It works!"
        }
        
        // Basic "Hello, world!" example
        router.get("hello") { req in
            return "Hello, world!"
        }
        
        // Example of configuring a controller
        let todoController = TodoController<DB>()
        router.get("todos", use: todoController.index)
        router.post("todos", use: todoController.create)
        router.delete("todos", Todo<DB>.parameter , use: todoController.delete)
        _ = MessageController(router: router, prefix: "message")
        _ = SecureController(router: router, prefix: "secure")

    }
}
