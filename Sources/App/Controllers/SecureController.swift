//
//  SecureController.swift
//  App
//
//  Created by Steve Clarke on 28/04/2019.
//

import Foundation

import Vapor
import Authentication
import FluentPostgreSQL

final class SecureController {
    let prefix : String
    
    init(router: Router, prefix: String) {
        self.prefix = prefix
        //router.grouped(Auth)
        router.get(prefix, use: get)
    }
    
    func get(_ req: Request) throws -> String {
        do {
            try  req.requireAuthenticated(User<PostgreSQLDatabase>.self)
        } catch {
            throw Abort(.unauthorized, reason: "Should have been authenticated")
        }
        return "Here is a message from SecureController"
    }
    
}
