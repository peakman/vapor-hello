//
//  MessageController.swift
//  App
//
//  Created by Steve Clarke on 16/01/2019.
//

import Vapor


struct Names : Codable, Content, ResponseEncodable, RequestCodable {
    var firstName: String
    var lastName: String
    var age: Int
}

final class MessageController {
    let prefix : String

    init(router: Router, prefix: String) {
        self.prefix = prefix
        router.get(prefix, use: get)
        router.post(prefix, use: post)
    }
    
    func get(_ req: Request) throws -> String {
        if let names = try? req.query.decode(Names.self) {
            return "\(names)"
        } else {
            return "Here is a message"
        }
    }
    func post(_ req: Request) throws -> Future<Names> {
        print(req.http.headers.first {$0.name == "Content-Type"}?.value as Any)
        return try req.content.decode(Names.self).map(to: Names.self) {namesIn  in
            let names = Names(firstName: "Susie",lastName: "RESPONSE", age: 72)
            print(namesIn)
            return names
        }
    }

}
