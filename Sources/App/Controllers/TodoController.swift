import Vapor
import Fluent

/// Controls basic CRUD operations on `Todo`s.
final class TodoController<DB> where DB: QuerySupporting {
    /// Returns a list of all `Todo`s.
    func index(_ req: Request) throws -> Future<[Todo<DB>]> {
        return Todo<DB>.query(on: req).all()
    }

    /// Saves a decoded `Todo` to the database.
    func create(_ req: Request) throws -> Future<Todo<DB>> {
        return try req.content.decode(Todo<DB>.self).flatMap { todo in
            if let tid = todo.id  {
                return Todo<DB>.find(tid, on: req).flatMap { todoq in
                    if todoq != nil {
                        return todo.save(on: req)
                    } else {
                        throw VaporError(identifier: "SAVE_ERROR", reason: "Attempt to update a todo \(tid) that does not exist")
                    }
                }
            } else {
                return todo.create(on: req)
            }
        }
    }

    /// Deletes a parameterized `Todo`.
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Todo<DB>.self).flatMap { todo in
            return todo.delete(on: req)
        }.transform(to: .ok)
    }
}
