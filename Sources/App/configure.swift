import FluentSQLite
import FluentPostgreSQL
import Vapor
import Authentication

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    /// Register providers first
    enum ActiveDB {
        case apsql, asqlite
    }
    let activeDB :  ActiveDB
    switch env {
    case .testing:
        try services.register(FluentSQLiteProvider())
        activeDB = .asqlite
    case .development, .production :
        activeDB = .apsql
        try services.register(FluentPostgreSQLProvider())
    default:
        fatalError("Environment \(env) not recognised" )
    }
    try services.register(AuthenticationProvider())
    // A dummy factory used to generate a BOOTED output for testing
    if env == .testing || env == .development{
        try services.register(RequestLoggerFactory())
    }

    /// Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    middlewares.use(RequestLoggerMiddleware(environment: env)) // Logs incoming requests
    services.register(middlewares)
    
    var commandConfig = CommandConfig.default()
    commandConfig.use(MigrateCommand.self, as: "migrate")
    commandConfig.use(RevertCommand.self, as: "revert")
    services.register(commandConfig)
    
    var databases = DatabasesConfig()
    var migrations = MigrationConfig()
    let router = EngineRouter.default()
    switch activeDB {
    case .asqlite:
        let defaultDB = try SQLiteDatabase(storage: .memory)
        databases.add(database: defaultDB , as: .sqlite)
        migrations.add(model: Todo<SQLiteDatabase>.self, database: .sqlite)
        let wrapper = RouterWrapper(router: router, db: defaultDB)
        try wrapper.routes()
    case .apsql:
        let db :PostgreSQLDatabase
        if let dburl : String = Environment.get("DATABASE_URL") {
            if let config = PostgreSQLDatabaseConfig(url: dburl) {
                db = PostgreSQLDatabase(config: config)
            } else {
                fatalError("Unable to create postgres db")
            }
        } else {
            db = PostgreSQLDatabase(config: PostgreSQLDatabaseConfig(hostname: "localhost", username: "scimeb", database: "hello" ))
        }
        databases.add(database: db , as: .psql)
        migrations.add(model: Todo.self, database: .psql)
        migrations.add(model: Person.self, database: .psql)
        migrations.add(model: User.self, database: .psql)

        let wrapper = RouterWrapper(router: router, db: db)
        try wrapper.routes()
    }
    services.register(router, as: Router.self)
    services.register(databases)
    services.register(migrations)
}





