//
//  RequestLogger.swift
//  App
//
//  Created by Steve Clarke on 19/01/2019.
//
import Vapor
// A dummy provider used solely to output the BOOTED string for testing
final class RequestLoggerFactory : Provider {
    func register(_ services: inout Services) throws {
        //Nothing to do
    }
    
    func willBoot(_ container: Container) throws -> EventLoopFuture<Void> {
        //print("LOGGING WILL BOOT")
        return .done(on: container)
    }
    
    func didBoot(_ container: Container) throws -> EventLoopFuture<Void> {
        print("\n\(bootedString)\n")
        return .done(on: container)
    }
}

protocol RequestLogger : Middleware {
    
}


final class RequestLoggerMiddleware : Middleware {
    
    func register(_ services: inout Services) throws {
        //Nothing to do
    }
    
    init(environment : Environment) {
        print("Init RequestLogger for \(environment.name)")
    }

    func respond(to request: Request, chainingTo next: Responder) throws -> EventLoopFuture<Response> {
        let logger = try? request.make(Logger.self)
        logger!.info("Logging a request: \(request.http.method) \(request.http.url)")
        return try next.respond(to: request)
    }
    
}
