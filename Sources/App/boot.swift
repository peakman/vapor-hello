import Vapor

let bootedString = "**-BOOTED-**"

/// Called after your application has initialized.
public func boot(_ app: Application) throws {
    if let logger = try? app.make(Logger.self) {
        logger.info("Booting my app: \(app.environment.name)")
        /*
        logger.info("About to throw a deliberate error")
        let x = logger as! Int
        logger.info("\(x)")
        logger.info("Threw a deliberate error")
        */
    } else {
        fatalError("Unable to make logger")
    }
}
