import FluentSQLite
import FluentPostgreSQL
import Vapor

/// A single entry of a Todo list.
final class Todo<DB>: Model where DB: QuerySupporting {
    static var idKey: WritableKeyPath<Todo<DB>, UUID?>{ return \.id }
    static var name: String {return "todo"}
    
    typealias Database = DB
    typealias ID = UUID
    
    /// The unique identifier for this `Todo`.
    var id: ID?

    /// A title describing what this `Todo` entails.
    var title: String

    /// Creates a new `Todo`.
    init(id: UUID? = nil, title: String) {
        self.id = id
        self.title = title
    }
}

extension Todo: Migration, AnyMigration where DB: SchemaSupporting & MigrationSupporting {
    /// See `Migration.prepare(on:)`
    static func prepare(on connection: DB.Connection) -> Future<Void> {
        return DB.create(self, on: connection) { builder in
            try addProperties(to: builder)
            //builder.reference(from: \.ownerID, to: \User<DB>.id)
        }
    }
    static func revert(on conn: DB.Connection) -> Future<Void> {
        return DB.delete(self, on: conn)
    }

}

/// Allows `Todo` to be encoded to and decoded from HTTP messages.
extension Todo: Content { }

/// Allows `Todo` to be used as a dynamic parameter in route definitions.
extension Todo: Parameter { }
