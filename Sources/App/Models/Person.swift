//
//  Person.swift
//  App
//
//  Created by Steve Clarke on 29/04/2019.
//

import FluentSQLite
import FluentPostgreSQL
import Vapor
import Authentication

/// A single entry of a Todo list.
final class Person<DB>: Model, Authenticatable where DB: QuerySupporting {
    
    static var idKey: WritableKeyPath<Person<DB>, Int?>{ return \.id }
    static var name: String {return "person"}
    
    typealias Database = DB
    typealias ID = Int
    
    /// The unique identifier for this `Person`.
    var id: ID? = nil
    
    var firstName: String
    var lastName : String
    var email : String?
    var phone : String
    var userId : Int?
    
    var usersSC : Children<Person<DB>, User<DB>> {
        return children(\.personId)
    }    
    /// Creates a new `Person`.
    init(id: Int? = nil, firstName: String, lastName: String, email: String? = nil, phone: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.phone = phone
    }
}

extension Person: Migration, AnyMigration where DB: SchemaSupporting & MigrationSupporting {
    /// See `Migration.prepare(on:)`
    static func prepare(on connection: DB.Connection) -> Future<Void> {
        let p = Person<DB>(
            firstName: "Jim",
            lastName: "Oliver",
            email: "jim@oliver.co.uk",
            phone: "07812 675439"
        )
        return DB.create(self, on: connection) { builder in
            try addProperties(to: builder)
            }.flatMap { _ in
                p.save(on: connection).flatMap() { (person) in
                    print("Person is \(person.firstName)")
                    return connection.future(())
            }
        }
    }
    static func revert(on conn: DB.Connection) -> Future<Void> {
        return DB.delete(self, on: conn)
    }
    
}

/// Allows `Person` to be encoded to and decoded from HTTP messages.
extension Person: Content { }

/// Allows `User` to be used as a dynamic parameter in route definitions.
extension Person: Parameter { }
