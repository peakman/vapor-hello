//
//  User.swift
//  App
//
//  Created by Steve Clarke on 28/04/2019.
//

import FluentSQLite
import FluentPostgreSQL
import Vapor
import Authentication

/// A single entry of a User
final class User<DB>: Model, Authenticatable where DB: QuerySupporting {
    
    static var idKey: WritableKeyPath<User<DB>, Int?>{ return \.id }
    static var name: String {return "user_sc"}
    
    typealias Database = DB
    typealias ID = Int
    
    /// The unique identifier for this `User`.
    var id: ID?
    
    let name: String
    let password : String
    let personId : Int?
    
    var person : Parent<User<DB>, Person<DB>>? {
        return parent(\.personId)
    }
    
    /// Creates a new `User`.
    init(id: Int? = nil, name: String, password: String) {
        self.id = id
        self.name = name
        self.password = password
        self.personId = nil
    }
}

extension User: Migration, AnyMigration where DB: SchemaSupporting & MigrationSupporting {
    /// See `Migration.prepare(on:)`
    static func prepare(on connection: DB.Connection) -> Future<Void> {
        return DB.create(self, on: connection) { builder in
            try addProperties(to: builder)
            builder.reference(from: \.personId, to: \Person<DB>.id)
        }
    }
    static func revert(on conn: DB.Connection) -> Future<Void> {
        return DB.delete(self, on: conn)
    }
    
}

/// Allows `User` to be encoded to and decoded from HTTP messages.
//extension User: Content { }

/// Allows `User` to be used as a dynamic parameter in route definitions.
//extension User: Parameter { }
